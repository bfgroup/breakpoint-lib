# Breakpoint-lib for React + TS

Библиотека предназначена для управления отображением компонента в зависимости от размеров экрана устройства.

## Установка библиотеки

`npm install https://alexsbf@bitbucket.org/alexsbf/breakpoint-lib.git`

## Использование

Для того, чтобы подключить какой-либо компонент к данной библиотеке, импортируем функцию **withBreakpoints** из пакета **breakpoints**

`import { withBreakpoints } from 'breakpoints'`  

Затем необходимо обернуть требуемый компонент функцией **withBreakpoints** с указанием типа пропсов, получаемых компонентом.

```
type BlockOwnProps = {
    todos: string[]
}

const Block: React.FC<BlockOwnProps> = ({todos}) => {

    return (
        <div className="Block">
            <p>Список дел, получаемых через props</p>
            <ul>
                {
                    todos.map(todo => (
                        <li key={Math.random()}>
                            {todo}
                        </li>
                    ))
                }
            </ul>
        </div>
    )
}

export default withBreakpoints<BlockOwnProps>(Block)
```
После этого, при использовании компонента **Block** мы должны передать ему объект **breakpoints** в качестве входного параметра:

```
<Block 
	todos={ ['task1'], ['task2'] }
	breakpoints={ {
		minHeight: 500,
		minWidth: 100
	} }
/>
```

Объект **breakpoints** может содержать от 0 до 4 свойств:

Имя свойства  | Тип | Описание
------------- | ------------- | -------------
**minWidth**,  **minHeight** | number  | При достижении этого значения компонент **начнет отображаться**
**maxWidth**, **maxHeight**  | number  | При достижении этого значения компонент **все еще будет отображаться**

Например, при одновременном задании свойств **minWidth: 300** и **maxWidth: 1200** компонент будет виден в диапазоне **300px-1200px** включительно

#### Пример экспорта компонента при использовании React + Redux-thunk + TS + Breakpoint-lib

```
export default connect<MapState, MapDispatch, BlockOwnProps, RootStateType>(mapStateToProps, mapDispatchToProps)(withBreakpoints<BlockProps>(Block))
```

В данном случае **BlockProps** является типом пересечения, т.е. 
```
type BlockProps = MapState & MapDispatch & BlockOwnProps
```
