import React, { useEffect, useState } from 'react'

export function withBreakpoints(Component) {

    const BreakPoint = ({breakpoints, ...props}) => {

        const [windowSize, SetWindowSize] = useState({
            width: window.innerWidth,
            height: window.innerHeight
        })

        const hasBreakpointsProp = breakpoints !== undefined
        let isShowedComponent = true

        let hasMinWidth = false
        let hasMinHeight = false
        let hasMaxWidth = false
        let hasMaxHeight = false

        if (hasBreakpointsProp) {
            hasMinWidth = breakpoints.hasOwnProperty('minWidth')
            hasMinHeight = breakpoints.hasOwnProperty('minHeight')
            hasMaxWidth = breakpoints.hasOwnProperty('maxWidth')
            hasMaxHeight = breakpoints.hasOwnProperty('maxHeight')
        }

        const minWidth = hasMinWidth ? breakpoints.minWidth - 1 : null
        const minHeight = hasMinHeight ? breakpoints.minHeight - 1 : null
        const maxWidth = hasMaxWidth ? breakpoints.maxWidth + 1 : null
        const maxHeight = hasMaxHeight ? breakpoints.maxHeight + 1 : null


        const recalcSizeWindow = () => {           
            let shouldUpdateState = false
            const newState = {...windowSize}
           
            const isChangedWidth = window.innerWidth !== newState.width
            const isChangedHeight = window.innerHeight !== newState.height
            let isDoubleEvent = false
            
            if (hasMinWidth && isChangedWidth) {                
                isDoubleEvent = window.innerWidth === window.innerHeight && window.innerWidth !== newState.width
                if (window.innerWidth <= minWidth && isShowedComponent) {
                    shouldUpdateState = true
                }
                if (window.innerWidth >= minWidth && !isShowedComponent) {
                    shouldUpdateState = true
                }
            }

            if (hasMaxWidth && isChangedWidth) {
                isDoubleEvent = window.innerWidth === window.innerHeight && window.innerWidth !== newState.width
                if (window.innerWidth >= maxWidth && isShowedComponent) {
                    shouldUpdateState = true
                }
                if (window.innerWidth <= maxWidth && !isShowedComponent) {
                    shouldUpdateState = true
                }
            }

            if (hasMinHeight && isChangedHeight) {
                if (window.innerHeight <= minHeight && isShowedComponent) {
                    shouldUpdateState = true
                }
                if (window.innerHeight >= minHeight && !isShowedComponent) {
                    shouldUpdateState = true
                }
            }

            if (hasMaxHeight && isChangedHeight) {
                if (window.innerHeight >= maxHeight && isShowedComponent) {
                    shouldUpdateState = true
                }
                if (window.innerHeight <= maxHeight && !isShowedComponent) {
                    shouldUpdateState = true
                }
            }

            if (shouldUpdateState) {
                if (isDoubleEvent) {
                    return
                }

                SetWindowSize({
                    ...newState,
                    width: window.innerWidth,
                    height: window.innerHeight
                })
            }
        }

        useEffect(() => {
            if (hasBreakpointsProp) {
                window.addEventListener('resize', recalcSizeWindow)
                return () => {
                    window.removeEventListener('resize', recalcSizeWindow)
                }
            }
        }, [windowSize])

        if (hasMinWidth && window.innerWidth <= minWidth) {
            isShowedComponent = false
            return null
        }

        if (hasMinHeight && window.innerHeight <= minHeight) {
            isShowedComponent = false
            return null
        }

        if (hasMaxWidth && window.innerWidth >= maxWidth) {
            isShowedComponent = false
            return null
        }

        if (hasMaxHeight && window.innerHeight >= maxHeight) {
            isShowedComponent = false
            return null
        }

        return (
            <Component {...props}/>
        )
    }
    return BreakPoint
}
